import tensorflow as tf
import numpy as np
import random

from tensorflow.keras.datasets import mnist

class DataGen(tf.keras.utils.Sequence):
    def __init__(self, batch_size, tr:bool=True, *args, **kwargs ):
        super(DataGen,self).__init__(*args, **kwargs)
        self.batch_size = batch_size
        self.tr = tr
        (trX, trY),(teX, teY)= tf.keras.datasets.mnist.load_data()

        self.X = trX if tr else teX
        self.Y = trY if tr else teY
        
        self.index = [i for i in range(self.X.shape[0])]
        self.on_epoch_end()

    def on_epoch_end(self):
        random.shuffle(self.index)

    def __len__(self):
        return int(len( self.index) // self.batch_size  ) 

    def __getitem__(self, index):
        index = self.index[index * self.batch_size : (index + 1) * self.batch_size]
        X = np.zeros((self.batch_size, *self.X.shape[1:], 1))
        Y = np.zeros((self.batch_size, 10))

        for i in range(self.batch_size):
                X[i, :] = np.expand_dims(self.X[index[i], :], axis=-1)
                Y[i, :] = tf.keras.utils.to_categorical(self.Y[index[i], :], num_classes=10)
        return X, Y
